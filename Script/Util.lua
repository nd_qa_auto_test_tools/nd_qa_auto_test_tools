
--[[***********工具函数脚本文件***************
                   _ooOoo_
                  o8888888o
                  88" . "88
                  (| -_- |)
                  O\  =  /O
               ____/`---'\____
             .'  \\|     |//  `.
            /  \\|||  :  |||//  \
           /  _||||| -:- |||||-  \
           |   | \\\  -  /// |   |
           | \_|  ''\---/''  |   |
           \  .-\__  `-`  ___/-. /
         ___`. .'  /--.--\  `. . __
      ."" '<  `.___\_<|>_/___.'  >'"".
     | | :  `- \`.;`\ _ /`;.`/ - ` : | |
     \  \ `-.   \_ __\ /__ _/   .-` /  /
======`-.____`-.___\_____/___.-`____.-'======
                   `=---='
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         佛祖保佑       永无BUG
]]


--找图例子
-- local bFind,x,y=dm:FindPic(0,0,2000,2000,GetPicturePath("xxx2.bmp"),"000000",0.9,0);
-- if bFind~=-1 then
	-- dm:MoveTo(x,y)
-- end

--调用游戏LUA
-- dm:ExecuteGameLua("SysMsgBox('call test asdfasdfasdfasdfas adsfasfdasdfasfsa','asdfasdf')")

--调用游戏LUA获取返回值
-- local nMoney=dm:ExecuteGameLuaRet("return GetPlayerMoney_lua()")

--*******************************************全局函数的定义******************************************************************


--获取自动工具目录的图片路径
--用法 GetPicturePath("xxx2.bmp") 得到E:\AutoTestTools\发布\Release\Img\xxx2.bmp
function GetPicturePath(PicFile)
	return dm:GetAppPath().."Img\\"..PicFile
end

function 获取玩家经验()
	return dm:ExecuteGameLuaRet("return GetPlayerExp_lua()")
end

function 获取玩家金币()
	local n=nil
	repeat
		n=dm:ExecuteGameLuaRet("return GetPlayerGold_lua()")
	until n~=nil
	return n
end

function 获取玩家铜币()
	local n=nil
	repeat
		n=dm:ExecuteGameLuaRet("return GetPlayerMoney_lua()")
	until n~=nil
	return n
end

function 获取玩家声望()
	local n=nil
	repeat
		n=dm:ExecuteGameLuaRet("return GetPlayerGeneralExp_lua()")
	until n~=nil
	return n
end

function 获取玩家军功()
	local n=nil
	repeat
		n=dm:ExecuteGameLuaRet("return GetPlayerExploit_lua()")
	until n~=nil
	return n
end

--比色封装，返回是否超时
-- local bTimeout=MyCmpColor(100,100,"ffffff",30000);
function MyCmpColor(x,y,color,timeout)
	local timeout = timeout or 10000000
	local timeValue = 0
	while true do
		if dm:CmpColor(x,y,color,0.9)==0 then
			return 1
		end
		dm:Delay(50)
		timeValue=timeValue+50
		if timeValue>timeout then
			return 0
		end
	end
end

--比色封装，返回是否超时和哪个颜色先出现
function MyCmpColorWhich(x1,y1,color1,x2,y2,color2,timeout)
	local timeout = timeout or 100000
	local timeValue = 0
	while true do
		if dm:CmpColor(x1,y1,color1,0.9)==0 then
			return 1
		end
		if dm:CmpColor(x2,y2,color2,0.9)==0 then
			return 2
		end
		dm:Delay(50)
		timeValue=timeValue+50
		if timeValue>timeout then
			return 0
		end
	end
end

--游戏中的某个窗口是否可见
--GameFrameIsVisible("NormalWaiting")
--GameFrameIsVisible("NormalMsgWindow")
function GameFrameIsVisible(FrameName)
	local n=nil
	repeat
		n=dm:ExecuteGameLuaRet(string.format('return (function() if IsVisible_lua("%s") then return 1 else return 0 end end)()',FrameName))
	until n~=nil
	return n == 1
end

--等待游戏窗口出现
function WaitGameFrameIsVisible(FrameName)
	local timeout = 20000		--超时20秒
	while not GameFrameIsVisible(FrameName) do
		dm:Delay(100)
		timeout=timeout-100
		if timeout<=0 then
			return false;
		end
	end
	return true;
end

function 获取士兵名(nRankId)
	local szScript=[[return (function()
		local szSodierName = ""
		szSodierName = GetDataFromIni_lua("rank_tree",%d).name or ""
		return szSodierName
	end)()
	]]
	local szRet = nil
	repeat
		szRet = dm:ExecuteGameLuaRet(string.format(szScript,nRankId))
	until szRet~=nil
	return szRet
end

function 获取物品名(nItemId)
	local szScript=[[return (function()
		local szItemName = ""
		local nScienceId = GetScienceId_lua(%d)
		local tData = GetDataFromIni_lua("itemtype",nScienceId)
		if nil ~= tData then
			szItemName = tData.name or ""
		end
		return szItemName
	end)()
	]]
	local szRet = nil
	repeat
		szRet = dm:ExecuteGameLuaRet(string.format(szScript,nItemId))
	until szRet~=nil
	return szRet
end

function 获取玩家昵称()
	return dm:ExecuteGameLuaRet("return GetPlayerName_lua()") or ""
end

--***********脚本工具函数模块***************
module("Util",package.seeall)

--获取仓库物品
function GetStoreItems()
	local szScript = [[
		local tItemList = ItemData:GetInstance():GetItemBagListSelLev1(1) or {}
		for _,tItem in ipairs(tItemList) do
			local tItemRemoteInfo = ItemData:GetInstance():GetItemInfoByID(tItem.id)
			local tItemLocalInfo = GetDataFromIni_lua("itemtype",tItemRemoteInfo.itemtype)
			tItem.pack_num = tItemRemoteInfo.pack_num			--叠加数
			tItem.level = tItemLocalInfo.level or 0
			tItem.name = tItemLocalInfo.name or ""
		end
		szData=table.serialize(tItemList)
		return szData
	]]
	local szData = dm:ExecuteGameLuaRet(szScript)
	local tData = loadstring(szData)()
	return tData
end

--显示游戏中转菊等待
function ShowGameNormalWaiting()
	dm:ExecuteGameLua('TK_FrameShow("NormalWaiting")');
end

--等待游戏中转菊结束
function WaitNormalWaiting()
	local timeout = 20000		--超时20秒
	while GameFrameIsVisible("NormalWaiting") do
		dm:Delay(100)
		timeout=timeout-100
		if timeout<=0 then
			return false;
		end
	end
	return true;
end

--在游戏窗口显示冒泡提示
function ShowBubbleTip(szText)
	local szScript = [[
		local szFrameName = "FrameBubbleTip_"..tostring(math.ramdom())
		local window = CreateFrame_lua(szFrameName,"Normal_Frame")
		GetWindowObj_lua("UIRoot"):AddChildWindow(window)
		SetProperty_lua(szFrameName,"Alpha","1")
		SetProperty_lua(szFrameName,"AlwaysOnTop","true")
		SetProperty_lua(szFrameName,"UnifiedWidth","{0,1280}")
		SetProperty_lua(szFrameName,"UnifiedHeight","{0,768}")
		SetProperty_lua(szFrameName,"UnifiedPosition","{{0,0},{0,0}")
		SetProperty_lua(szFrameName,"Visible","true")
		SetProperty_lua(szFrameName,"UnifiedHeight","{0,768}")
		SetText_lua(szFrameName,"%s")
		TK_FrameShow(szFrameName)
		
		local actMove = ActionMoveTo:ActionWithDuration(1,StringToUVector2("{{0,0},{0,-100}}"))
		-- local actFade = ActionFadeTo:ActionWithDuration(1,0)
		local actRemove = ActionCallback:ActionWithTarget(function() GetWindowObj_lua("UIRoot"):RemoveChildWindow(window) end,nil)
		local sequence = ActionSequence:Actions(actMove,actRemove)
		RunAction_lua(szFrameName,sequence)
	]]
	dm:ExecuteGameLua(string.format(szScript,dm:UTF8(szText)))
end