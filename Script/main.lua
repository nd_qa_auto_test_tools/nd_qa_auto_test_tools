--*************************脚本主入口文件********************

--获取大漠插件，保存在全局变量dm里面
dm=CDMPluginWraper:GetInstance();
hHubaoqiFrontendWnd = dm:GetForegroundWindow()
nScreenWidth,nScreenHeight = dm:GetClientSize(hHubaoqiFrontendWnd)
-- ndlog
ndlog = function(...)
    dm:Print(string.format(...))
end

-- 输出绑定执行函数发生错误的信息
function __G__TRACKBACK__(msg)
    ndlog("----------------------------------------")
    ndlog("LUA ERROR: " .. tostring(msg) .. "\n")
    ndlog(debug.traceback())
    ndlog("----------------------------------------")
    return msg
end

local function main()
    collectgarbage("collect")
    -- avoid memory leak 这是脚本回收参数，避免内存泄漏
    collectgarbage("setpause", 100)
    collectgarbage("setstepmul", 5000)
	
	math.randomseed(os.time())

	--添加脚本路径
	local path = dm:GetAppPath().."Script\\?.lua;"
	package.path = package.path ..";".. path
	
	require "serialize"
	require "Util"
	
	-- 绑定窗口
	local bSucess=dm:BindWindow(hHubaoqiFrontendWnd,"dx", "dx", "dx", 0)
	if bSucess == 0 then
		ndlog("绑定游戏窗口失败")
		return;
	end
	
--*****************不要修改以上部分****************************
	
--*****************可以修改部分开始****************************
	
	--下面执行需要执行的脚本文件
	
	--设置日志标签
	dm:SetLogTag(获取玩家昵称())
	
	require(dm:GetExecScriptFile())

	
--*****************可以修改部分结束****************************

	dm:UnBindWindow()

end

--[[
xpcall( 调用函数, 错误捕获函数 );
lua提供了xpcall来捕获异常
xpcall接受两个参数:调用函数、错误处理函数。
当错误发生时,Lua会在栈释放以前调用错误处理函数,因此可以使用debug库收集错误相关信息。
两个常用的debug处理函数:debug.debug和debug.traceback
前者给出Lua的提示符,你可以自己动手察看错误发生时的情况;
后者通过traceback创建更多的错误信息,也是控制台解释器用来构建错误信息的函数。
--]]
local status, msg = xpcall(main, __G__TRACKBACK__)
if not status then
    error(msg)
end