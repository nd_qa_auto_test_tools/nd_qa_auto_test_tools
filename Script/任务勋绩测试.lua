--[[
                   _ooOoo_
                  o8888888o
                  88" . "88
                  (| -_- |)
                  O\  =  /O
               ____/`---'\____
             .'  \\|     |//  `.
            /  \\|||  :  |||//  \
           /  _||||| -:- |||||-  \
           |   | \\\  -  /// |   |
           | \_|  ''\---/''  |   |
           \  .-\__  `-`  ___/-. /
         ___`. .'  /--.--\  `. . __
      ."" '<  `.___\_<|>_/___.'  >'"".
     | | :  `- \`.;`\ _ /`;.`/ - ` : | |
     \  \ `-.   \_ __\ /__ _/   .-` /  /
======`-.____`-.___\_____/___.-`____.-'======
                   `=---='
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         佛祖保佑       永无BUG
]]

--***********任务和勋绩测试模块***************
module("MissionAndAchievementTest",package.seeall)

--勋章领奖状态
local MEDAL_STATE = {
	NONE 	= 0 , -- 不可领奖
	CANGET 	= 1 , -- 可领奖
	HAVEGET	= 2 , -- 已领取奖励
}

local OTHER_AWARD = {
	["copper"] =  "铜币",
	["gold"] = "金币", 
	["lieutenantexp"] = "副将经验", 
	["generalexp"] = "声望",
	["military"] = "勋绩",
	["soldiersexp"] = "士兵经验",
	["prestige"] = "声望",
}

--获得所有勋章信息
function GetMergingAlllMedalInfo()
	local szScript = [[
		local tData = GetMergingAlllMedalInfo() or {}
		szData=table.serialize(tData)
		return szData
	]]
	local szData = dm:ExecuteGameLuaRet(szScript)
	local tData = loadstring(szData)()
	return tData
end

--PM命令激活勋章
function ActivateMedalById(nMedalId)
	dm:ExecuteGameLua(string.format('lua_console("achievemedal %d")',nMedalId))
end

--PM命令完成任务
function ReqCompleteTaskById(nTaskId)
	dm:ExecuteGameLua(string.format('lua_console("achievetask %d")',nTaskId))
end

--获取玩家荣誉信息
function GetUserHonorInfo()
	local szScript = [[
		local tData = GetUserHonorInfo() or {}
		szData=table.serialize(tData)
		return szData
	]]
	local szData = dm:ExecuteGameLuaRet(szScript)
	local tData = loadstring(szData)()
	return tData
end

--重写游戏中获得勋章回调函数
function Override_On_MedalGet()
	local szScript = [[
		function On_MedalGet( param )
			TK_FrameHide("NormalWaiting")
		end
	]]
	dm:ExecuteGameLua(szScript)
end

--重写游戏脚本任务完成回调函数
function Override_On_Task_CallBack_QuestFinish()
	local szScript = [[
		Origin_Task_CallBack_QuestFinish = Task_CallBack_QuestFinish
		Task_CallBack_QuestFinish = function(args) 
			Origin_Task_CallBack_QuestFinish(args)
			TK_FrameHide("NormalWaiting")
		end
	]]
	dm:ExecuteGameLua(szScript)
end

--还原
function Restore_On_Task_CallBack_QuestFinish()
	local szScript = [[
		Task_CallBack_QuestFinish = Origin_Task_CallBack_QuestFinish
	]]
	dm:ExecuteGameLua(szScript)
end

--获取勋章的奖励
function GetMedalReward(tMedalInfo)
	local tAwardInfo = {}
	local tOtherAward = {}
	local nHonorValue = 0
	for i =1,7 do
		local tvalueInfo = {}
		local sType = tMedalInfo["award_key_" .. i]
		local sValue = tMedalInfo["award_value_" .. i]	
		if sType ~= nil and sValue ~= nil and sType ~= "" and sValue ~= "" then
			if sType == "honornum" then 
				nHonorValue = nHonorValue + tonumber(sValue)
			elseif sType == "items" then 
				if tonumber(sValue) ~= 0 then
					tvalueInfo["type"] = sType
					tvalueInfo["value"] = tonumber(sValue)
					table.insert(tAwardInfo,tvalueInfo)
				end
			else
				table.insert(tOtherAward,{["type"]=sType,["value"]=tonumber(sValue)})
			end
		end
	end
	if #tOtherAward > 0 then
		local tvalueInfo = {}
		tvalueInfo["type"] = "otherAward"
		tvalueInfo["value"] = tOtherAward
		table.insert(tAwardInfo,1,tvalueInfo)
	end
	return nHonorValue,tAwardInfo
end

--获取勋章的物品奖励信息
function GetMedalAwardItemInfo(tAward)
	if tAward.type ~= "items" then
		return nil
	end
	local szScript = [[
		local tItemInfo = GetDataFromIni_lua("itemtype",%d)
		tItemInfo.szItemName = Get_LanguageStr("Achievement",tItemInfo.name) or ""
		szData=table.serialize(tItemInfo)
		return szData
	]]
	
	local szData = dm:ExecuteGameLuaRet(string.format(szScript,tAward.value))
	local tData = loadstring(szData)()
	return tData
	
end

--勋章领奖
function MedalGetReward( nMedalId )
	local szScript = [[
		LobbyClient:GetInstance():Command_GetMedalAward(%d)
		TK_FrameShow("NormalWaiting")
	]]
	dm:ExecuteGameLua(string.format(szScript,nMedalId))
end

--激活勋章
function ActivateMedal(tMedalInfo)
	if tMedalInfo["ifinishTime"] ~= nil and tMedalInfo["ifinishTime"] > 0 then
		return
	end
	local nPreUserHonor = GetUserHonorInfo().honorvalue
			
	-- 显示转菊
	Util.ShowGameNormalWaiting()
	
	-- 激活勋章
	ActivateMedalById(tMedalInfo.id)
	
	-- 等待转菊消失
	Util.WaitNormalWaiting()
	
	--打印日志
	local nAwardHonor,_ = GetMedalReward(tMedalInfo)
	local szDebugInfo = string.format("激活勋章id:%d,name:%s,荣誉/获得:%d/%d",tMedalInfo.id,tMedalInfo.name,nAwardHonor,GetUserHonorInfo().honorvalue-nPreUserHonor)
	
	ndlog(szDebugInfo)
	Util.ShowBubbleTip(szDebugInfo)
end

--获取玩家数值
function GetProtertyValue(tAward)
	if tAward.type == "copper" then
		return 获取玩家铜币()
	elseif tAward.type == "gold" then
		return 获取玩家金币()
	elseif tAward.type == "generalexp" then
		return 获取玩家声望()
	else
		return 0
	end
end

--获取仓库某个物品的数量
function GetStoreItemAmount(tStoreItemList,szItemName)
	local nRet = 0
	for _,tItem in ipairs(tStoreItemList) do
		if tItem.name == szItemName then
			nRet = nRet + tItem.pack_num
		end
	end
	return nRet
end

--获取日常任务
function GetDailyTask()
	local szScript = [[
		local tDailyTaskId = TaskData:GetInstance():GetDailyTaskInfo() or {}
		local tData = {}
		for _,nTaskId in ipairs(tDailyTaskId) do
			local tTaskRemoteInfo = TaskData:GetInstance():GetTaskInfoByID(nTaskId)
			local tTaskLocalInfo = GetBinariesTabFilePV1_lua("fr_task","id",tostring(nTaskId))
			if tTaskLocalInfo then
				tTaskLocalInfo = tTaskLocalInfo[1]
			end
			if tTaskLocalInfo  and tTaskRemoteInfo then
				
				if tTaskLocalInfo.award_item1 and tTaskLocalInfo.award_item1 ~= 0 then
					local tItemInfo = GetDataFromIni_lua("itemtype",tTaskLocalInfo.award_item1)
					if tItemInfo then
						tTaskLocalInfo.award_item1_name = tItemInfo.name
					end
				end
				if tTaskLocalInfo.award_item2 and tTaskLocalInfo.award_item2 ~= 0 then
					local tItemInfo = GetDataFromIni_lua("itemtype",tTaskLocalInfo.award_item2)
					if tItemInfo then
						tTaskLocalInfo.award_item2_name = tItemInfo.name
					end
				end
				if tTaskLocalInfo.award_item3 and tTaskLocalInfo.award_item3 ~= 0 then
					local tItemInfo = GetDataFromIni_lua("itemtype",tTaskLocalInfo.award_item3)
					if tItemInfo then
						tTaskLocalInfo.award_item3_name = tItemInfo.name
					end
				end
				if tTaskLocalInfo.award_item4 and tTaskLocalInfo.award_item4 ~= 0 then
					local tItemInfo = GetDataFromIni_lua("itemtype",tTaskLocalInfo.award_item4)
					if tItemInfo then
						tTaskLocalInfo.award_item4_name = tItemInfo.name
					end
				end
				
				for k,v in pairs(tTaskRemoteInfo) do
					tTaskLocalInfo[k]=v
				end
				
				tTaskLocalInfo.desc=" "
				tTaskLocalInfo.act1_desc=" "
				tTaskLocalInfo.act2_desc=" "
				tTaskLocalInfo.act3_desc=" "
				
				table.insert(tData,tTaskLocalInfo)
			end
			
		end
		szData=table.serialize(tData)
		return szData
	]]
	
	local szData = dm:ExecuteGameLuaRet(szScript)
	local tData = loadstring(szData)()
	return tData
end

--运行测试
function RunTest(tTestInfo)
	ndlog("------------------【%s】测试开始------------------",tTestInfo.szName)
	
	tTestInfo.fTestFunc()
	
	ndlog("------------------【%s】测试结束------------------",tTestInfo.szName)
end

--勋绩测试
function AchievementTest()
	
	Override_On_MedalGet()
	
	local tAlllMedalInfo = GetMergingAlllMedalInfo()

	--解锁
	for _,v in pairs(tAlllMedalInfo) do
		--激活勋章
		ActivateMedal(v)
	end
	
	--领取奖励
	
	tAlllMedalInfo = GetMergingAlllMedalInfo()
	
	for _,v in pairs(tAlllMedalInfo) do
	
		local _,tAwardItem = GetMedalReward(v)
		
		if v.iState == MEDAL_STATE.CANGET and #tAwardItem > 0 then
			
			--领奖前先保存玩家数值信息
			local tPrePropertyValue = {
				["copper"] =  获取玩家铜币(),
				["gold"] = 获取玩家金币(), 
				["lieutenantexp"] = 0, 
				["generalexp"] = 获取玩家声望(),
				["military"] = 0,
				["soldiersexp"] = 0,
				["prestige"] = 0,
			}
			
			--获取仓库物品列表
			local tPreStoreItemList = Util.GetStoreItems()
			
			--领取奖励
			MedalGetReward(v.id)
			
			-- 等待转菊消失
			Util.WaitNormalWaiting()
			
			--打印日志
			ndlog(string.format("-------------------勋章物品奖励开始:%s-------------------",v.name))
			for _,tAward in ipairs(tAwardItem) do
				if tAward.type == "items" then
				
					--物品奖励
					local tItemInfo = GetMedalAwardItemInfo(tAward)
					local tNowStoreItemList = Util.GetStoreItems()
					
					--比较获取奖励之前和之后对应物品的的数量
					local nPreItemAmount = GetStoreItemAmount(tPreStoreItemList,tItemInfo.szItemName)
					local nNowItemAmount = GetStoreItemAmount(tNowStoreItemList,tItemInfo.szItemName)
					
					local szResult = "失败"
					if nNowItemAmount > nPreItemAmount then
						szResult = "成功"
					end
					
					local szDebugInfo = string.format("领取奖励:%s,获得%s",tItemInfo.szItemName,szResult);
					ndlog(szDebugInfo)
				elseif tAward.type == "otherAward" then
				
					--金币声望这些数值奖励
					for _,tOtherAward in ipairs(tAward.value) do
						local nAfterValue = GetProtertyValue(tOtherAward)
						local szDebugInfo = string.format("领取奖励:%s,奖励/获得:%d/%d",OTHER_AWARD[tOtherAward.type],tOtherAward.value,nAfterValue - tPrePropertyValue[tOtherAward.type]);
						ndlog(szDebugInfo)
					end
				end
			end
			ndlog(string.format("-------------------勋章物品奖励结束:%s-------------------",v.name))
		end
		
	end
	
end

--任务测试
function MissionTest()
	
	Override_On_Task_CallBack_QuestFinish()
	
	local tDailyTask = GetDailyTask()
	for _,tTaskInfo in ipairs(tDailyTask) do
		if tTaskInfo.complete == 0 then
		
			--领奖前先保存玩家数值信息
			local tPrePropertyValue = {}
			if tTaskInfo.award_money and tTaskInfo.award_money > 0 then
				tPrePropertyValue["nPreCopper"] = 获取玩家铜币()
			end
			if tTaskInfo.award_generalexp and tTaskInfo.award_generalexp > 0 then
				tPrePropertyValue["nPreGeneralExp"] = 获取玩家声望()
			end
			if tTaskInfo.award_exploit and tTaskInfo.award_exploit > 0 then
				tPrePropertyValue["nPreExploit"] = 获取玩家军功()
			end
			if tTaskInfo.award_exp and tTaskInfo.award_exp > 0 then
				tPrePropertyValue["nPreExp"] = 获取玩家经验()
			end
			if tTaskInfo.award_gold and tTaskInfo.award_gold > 0 then
				tPrePropertyValue["nPreGold"] = 获取玩家金币()
			end
			
			--获取仓库物品列表
			local tPreStoreItemList = Util.GetStoreItems()
			
			-- 显示转菊
			Util.ShowGameNormalWaiting()
			
			-- 请求完成任务
			ReqCompleteTaskById(tTaskInfo.id)
			
			-- 等待转菊消失
			Util.WaitNormalWaiting()
			
			local szDebugInfo = ""
			
			--数值类奖励
			if tTaskInfo.award_money and tTaskInfo.award_money > 0 then
				szDebugInfo = szDebugInfo..string.format("  铜币/获得:%d/%d",tTaskInfo.award_money,获取玩家铜币() - tPrePropertyValue["nPreCopper"])
			end
			if tTaskInfo.award_generalexp and tTaskInfo.award_generalexp > 0 then
				szDebugInfo = szDebugInfo..string.format("  声望/获得:%d/%d",tTaskInfo.award_generalexp,获取玩家声望() - tPrePropertyValue["nPreGeneralExp"])
			end
			if tTaskInfo.award_exploit and tTaskInfo.award_exploit > 0 then
				szDebugInfo = szDebugInfo..string.format("  军功/获得:%d/%d",tTaskInfo.award_exploit,获取玩家军功() - tPrePropertyValue["nPreExploit"])
			end
			if tTaskInfo.award_exp and tTaskInfo.award_exp > 0 then
				szDebugInfo = szDebugInfo..string.format("  经验/获得:%d/%d",tTaskInfo.award_exp,获取玩家经验() - tPrePropertyValue["nPreExp"])
			end
			if tTaskInfo.award_gold and tTaskInfo.award_gold > 0 then
				szDebugInfo = szDebugInfo..string.format("  金币/获得:%d/%d",tTaskInfo.award_gold,获取玩家金币() - tPrePropertyValue["nPreGold"])
			end
			
			--物品奖励
			local tNowStoreItemList = Util.GetStoreItems()
			if tTaskInfo.award_item1 and tTaskInfo.award_item1 ~= 0 then
				local szItemName = tTaskInfo.award_item1_name
				local nPreItemAmount = GetStoreItemAmount(tPreStoreItemList,szItemName)
				local nNowItemAmount = GetStoreItemAmount(tNowStoreItemList,szItemName)
				if nNowItemAmount - nPreItemAmount == tTaskInfo.award_item1_num then
					szDebugInfo = szDebugInfo..string.format("  奖励物品:%s,获得成功",szItemName)
				else
					szDebugInfo = szDebugInfo..string.format("  奖励物品:%s,获得失败",szItemName)
				end
			end
			if tTaskInfo.award_item2 and tTaskInfo.award_item2 ~= 0 then
				local szItemName = tTaskInfo.award_item2_name
				local nPreItemAmount = GetStoreItemAmount(tPreStoreItemList,szItemName)
				local nNowItemAmount = GetStoreItemAmount(tNowStoreItemList,szItemName)
				if nNowItemAmount - nPreItemAmount == tTaskInfo.award_item2_num then
					szDebugInfo = szDebugInfo..string.format("  奖励物品:%s,获得成功",szItemName)
				else
					szDebugInfo = szDebugInfo..string.format("  奖励物品:%s,获得失败",szItemName)
				end
			end
			if tTaskInfo.award_item3 and tTaskInfo.award_item3 ~= 0 then
				local szItemName = tTaskInfo.award_item3_name
				local nPreItemAmount = GetStoreItemAmount(tPreStoreItemList,szItemName)
				local nNowItemAmount = GetStoreItemAmount(tNowStoreItemList,szItemName)
				if nNowItemAmount - nPreItemAmount == tTaskInfo.award_item3_num then
					szDebugInfo = szDebugInfo..string.format("  奖励物品:%s,获得成功",szItemName)
				else
					szDebugInfo = szDebugInfo..string.format("  奖励物品:%s,获得失败",szItemName)
				end
			end
			if tTaskInfo.award_item4 and tTaskInfo.award_item4 ~= 0 then
				local szItemName = tTaskInfo.award_item4_name
				local nPreItemAmount = GetStoreItemAmount(tPreStoreItemList,szItemName)
				local nNowItemAmount = GetStoreItemAmount(tNowStoreItemList,szItemName)
				if nNowItemAmount - nPreItemAmount == tTaskInfo.award_item4_num then
					szDebugInfo = szDebugInfo..string.format("  奖励物品:%s,获得成功",szItemName)
				else
					szDebugInfo = szDebugInfo..string.format("  奖励物品:%s,获得失败",szItemName)
				end
			end
			
			ndlog(szDebugInfo)
		end
	end
	
	Restore_On_Task_CallBack_QuestFinish()
end

--测试系统名称和函数配置
tTest = {}
tTest["AchievementTest"] = { szName = "勋绩测试" , fTestFunc = AchievementTest }
tTest["MissionTest"]	 = { szName = "任务测试" , fTestFunc = MissionTest }

repeat
	--如果勋绩界面没有显示，那么打开
	if not GameFrameIsVisible("Achievement") then
		
		dm:MoveTo(25,84)
		dm:LeftClick()

		-- 判断窗口是否打开
		if MyCmpColor(1243,101,"d9d1ba",3000)==0 then
			ndlog("打开玩家详情窗口失败")
			break;
		end
		dm:Delay(1000)
		dm:MoveTo(181,108)
		dm:LeftClick()
		
		if MyCmpColor(181,108,"7997bf",3000)==0 then
			ndlog("打开勋绩窗口失败")
			break;
		end

		dm:Delay(1000)
		
	end
	
	--执行测试函数
	RunTest(tTest.AchievementTest);
	
	dm:Delay(2000)
	
	RunTest(tTest.MissionTest);
until true;
