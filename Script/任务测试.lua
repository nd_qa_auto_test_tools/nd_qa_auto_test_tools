--[[
                   _ooOoo_
                  o8888888o
                  88" . "88
                  (| -_- |)
                  O\  =  /O
               ____/`---'\____
             .'  \\|     |//  `.
            /  \\|||  :  |||//  \
           /  _||||| -:- |||||-  \
           |   | \\\  -  /// |   |
           | \_|  ''\---/''  |   |
           \  .-\__  `-`  ___/-. /
         ___`. .'  /--.--\  `. . __
      ."" '<  `.___\_<|>_/___.'  >'"".
     | | :  `- \`.;`\ _ /`;.`/ - ` : | |
     \  \ `-.   \_ __\ /__ _/   .-` /  /
======`-.____`-.___\_____/___.-`____.-'======
                   `=---='
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         佛祖保佑       永无BUG
]]

--***********任务测试模块***************
module("MissionTest",package.seeall)

--勋章领奖状态
local MEDAL_STATE = {
	NONE 	= 0 , -- 不可领奖
	CANGET 	= 1 , -- 可领奖
	HAVEGET	= 2 , -- 已领取奖励
}

local OTHER_AWARD = {
	["copper"] =  "铜币",
	["gold"] = "金币", 
	["lieutenantexp"] = "副将经验", 
	["generalexp"] = "声望",
	["military"] = "勋绩",
	["soldiersexp"] = "士兵经验",
	["prestige"] = "声望",
}

--获得所有勋章信息
function GetMergingAlllMedalInfo()
	local szScript = [[
		local tData = GetMergingAlllMedalInfo() or {}
		szData=table.serialize(tData)
		return szData
	]]
	local szData = dm:ExecuteGameLuaRet(szScript)
	local tData = loadstring(szData)()
	return tData
end

--激活勋章
function ActivateMedalById(nMedalId)
	dm:ExecuteGameLua(string.format('lua_console("achievemedal %d")',nMedalId))
end

--获取玩家荣誉信息
function GetUserHonorInfo()
	local szScript = [[
		local tData = GetUserHonorInfo() or {}
		szData=table.serialize(tData)
		return szData
	]]
	local szData = dm:ExecuteGameLuaRet(szScript)
	local tData = loadstring(szData)()
	return tData
end

--重写游戏中获得勋章回调函数
function Override_On_MedalGet()
	local szScript = [[
		function On_MedalGet( param )
			TK_FrameHide("NormalWaiting")
		end
	]]
	dm:ExecuteGameLua(szScript)
end

--重写游戏脚本获得勋章奖励回调函数
function Override_On_MedalAward()
	local szScript = [[
		function Fake_On_MedalAward( param )
			On_MedalAward(param)
		end
		LobbyClient:GetInstance():RegisterCallBack(262, "Fake_On_MedalAward") --勋章奖励
	]]
	dm:ExecuteGameLua(szScript)
end

--获取勋章的奖励
function GetMedalReward(tMedalInfo)
	local tAwardInfo = {}
	local tOtherAward = {}
	local nHonorValue = 0
	for i =1,7 do
		local tvalueInfo = {}
		local sType = tMedalInfo["award_key_" .. i]
		local sValue = tMedalInfo["award_value_" .. i]	
		if sType ~= nil and sValue ~= nil and sType ~= "" and sValue ~= "" then
			if sType == "honornum" then 
				nHonorValue = nHonorValue + tonumber(sValue)
			elseif sType == "items" then 
				if tonumber(sValue) ~= 0 then
					tvalueInfo["type"] = sType
					tvalueInfo["value"] = tonumber(sValue)
					table.insert(tAwardInfo,tvalueInfo)
				end
			else
				table.insert(tOtherAward,{"type"=sType,"value"=tonumber(sValue)})
			end
		end
	end
	if #tOtherAward > 0 then
		local tvalueInfo = {}
		tvalueInfo["type"] = "otherAward"
		tvalueInfo["value"] = tOtherAward
		table.insert(tAwardInfo,1,tvalueInfo)
	end
	return nHonorValue,tAwardInfo
end

--获取勋章的物品奖励信息
function GetMedalAwardItemInfo(tAward)
	if tAward.type ~= "items" then
		return nil
	end
	local szScript = [[
		local tItemInfo = GetDataFromIni_lua("itemtype",%d)
		tItemInfo.szItemName = Get_LanguageStr("Achievement",tItemInfo.name) or ""
		szData=table.serialize(tItemInfo)
		return szData
	]]
	
	local szData = dm:ExecuteGameLuaRet(string.format(szScript,tAward.value))
	local tData = loadstring(szData)()
	return tData
	
end

--勋章领奖
function MedalGetReward( nMedalId )
	local szScript = [[
		LobbyClient:GetInstance():Command_GetMedalAward(%d)
		TK_FrameShow("NormalWaiting")
	]]
	dm:ExecuteGameLua(string.format(szScript,nMedalId))
end

--激活勋章
function ActivateMedal(tMedalInfo,nAwardHonor)
	if tMedalInfo["ifinishTime"] ~= nil and tMedalInfo["ifinishTime"] > 0 then
		return
	end
	local nPreUserHonor = GetUserHonorInfo().honorvalue
			
	-- 显示转菊
	Util.ShowGameNormalWaiting()
	
	-- 激活勋章
	ActivateMedalById(tMedalInfo.id)
	
	-- 等待转菊消失
	Util.WaitNormalWaiting()
	
	--打印日志
	local szDebugInfo = string.format("激活勋章id:%d,name:%s,荣誉/获得:%d/%d",tMedalInfo.id,tMedalInfo.name,nAwardHonor,GetUserHonorInfo().honorvalue-nPreUserHonor)
	
	ndlog(szDebugInfo)
	Util.ShowBubbleTip(szDebugInfo)
end

--获取数值
function GetProtertyValue(tAward)
	if tAward.type == "copper" then
		return 获取玩家铜币()
	elseif tAward.type == "gold" then
		return 获取玩家金币()
	elseif tAward.type == "generalexp" then
		return 获取玩家声望()
	else then
		return 0
	end
end

--运行测试
function RunTest(tTestInfo)
	ndlog("------------------【%s】测试开始------------------",tTestInfo.szName)
	
	tTestInfo.fTestFunc()
	
	ndlog("------------------【%s】测试结束------------------",tTestInfo.szName)
end

--勋绩测试
function AchievementTest()
	
	Override_On_MedalGet()
	
	local tAlllMedalInfo = GetMergingAlllMedalInfo()

	--解锁
	for _,v in pairs(tAlllMedalInfo) do
	
		local nAwardHonor,tAwardItem = GetMedalReward(v)
		
		--激活勋章
		ActivateMedal(v,nAwardHonor)
		
		if v.iState == MEDAL_STATE.CANGET and #tAwardItem > 0 then
			
			local tPrePropertyValue = {
				["copper"] =  获取玩家铜币(),
				["gold"] = 获取玩家金币(), 
				["lieutenantexp"] = 0, 
				["generalexp"] = 获取玩家声望(),
				["military"] = 0,
				["soldiersexp"] = 0,
				["prestige"] = 0,
			}
			
			--领取奖励
			MedalGetReward(v.id)
			
			-- 等待转菊消失
			Util.WaitNormalWaiting()
			
			--打印日志
			for _,tAward in ipairs(tAwardItem) do
				if tAward.type == "items" then
					local tItemInfo = GetMedalAwardItemInfo(tAward)
					ndlog(tItemInfo.szItemName)
					local szDebugInfo = string.format("领取奖励:%s,获得:%s",tItemInfo.szItemName,"");
					ndlog(szDebugInfo)
				elseif tAward.type == "otherAward" then
					for _,tOtherAward in ipairs(tAward.value) do
						local nAfterValue = GetProtertyValue(tOtherAward)
						local szDebugInfo = string.format("领取奖励:%s,奖励/获得:%d/%d",OTHER_AWARD[tOtherAward.type],tOtherAward.value,nAfterValue - tPrePropertyValue[tOtherAward.type]);
						ndlog(szDebugInfo)
					end
				end
			end
			
		end
		
	end
	
end

--任务测试
function MissionTest()
	
end

--测试系统名称和函数配置
tTest = {}
tTest["AchievementTest"] = { szName = "勋绩测试" , fTestFunc = AchievementTest }
tTest["MissionTest"]	 = { szName = "任务测试" , fTestFunc = MissionTest }


--执行测试函数
RunTest(tTest.AchievementTest);