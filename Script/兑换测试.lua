

--一定要等到兑换窗口出现才可调用这个函数
local function 兑换窗口输入数值(数值)
	-- 删除并输入要兑换的数量
	dm:MoveTo(598,389)
	dm:LeftDown()
	dm:Delay(1000)
	dm:MoveTo(398,389)
	dm:LeftUp()
	dm:Delay(1000)
	
	--输入值
	dm:SendString(hHubaoqiFrontendWnd,tostring(数值))
	
	--点击兑换
	dm:MoveTo(770,580)
	dm:LeftClick()
	dm:Delay(1000)
end

--负数兑换测试
--返回值 是否通过
-- local function 负数兑换测试()
	-- --请求兑换-1个金币
	-- dm:ExecuteGameLua("Exchange_Money(-1)")
	
	-- --是否出现兑换异常对话框
	-- if MyCmpColor(457,214,"DB635D",30000)==0 then
		-- ndlog("【负数兑换测试】没有出现兑换异常对话框")
		-- return false;
	-- end
	-- ndlog("【负数兑换测试】测试通过")
	
	-- --移动到确定按钮并点击
	-- dm:MoveTo(558,442)
	-- dm:LeftClick()
	
	
	
	-- return true
-- end

--铜币上限兑换测试
--返回值 是否通过
local function 铜币上限兑换测试()
	
	兑换窗口输入数值(获取玩家金币())
	
	--是否出现兑换异常对话框
	if MyCmpColor(457,214,"DB635D",30000)==0 then
		ndlog("【铜币上限兑换测试】没有出现兑换异常对话框")
		return false;
	end
	ndlog("【铜币上限兑换测试】测试通过")
	
	--移动到确定按钮并点击
	dm:MoveTo(558,442)
	dm:LeftClick()
	
	return true
end

--铜币兑换测试
local function 铜币兑换测试(测试次数)
	
	--在游戏窗口按开始脚本
	
	-- local _,nScreenWidth,nScreenHeight = dm:GetClientSize(hHubaoqiFrontendWnd)
	-- ndlog(string.format("游戏窗口大小：width:%d,height:%d",nScreenWidth,nScreenHeight))

	--移动到兑换按钮并点击
	dm:MoveTo(32,30)
	dm:LeftClick()
	
	-- 判断窗口是否打开
	if MyCmpColor(360,190,"758AA6",30000)==0 then
		ndlog("打开兑换窗口失败")
		return;
	end
	
	--初始化随机数种子
	math.randomseed(os.time())
	
	if not 铜币上限兑换测试() then
		return;
	end
	
	for nCount=1,测试次数 do
		
		local nJinbi=获取玩家金币()		--金币
		local nTongbi=获取玩家铜币()	--铜币
		
		ndlog(string.format("【测试铜钱兑换】当前%d/%d,金币/铜币：%d/%d",nCount,测试次数,nJinbi,nTongbi))
		
		local nValue=math.random(1,2000)
		兑换窗口输入数值(nValue)
		
		-- 等待兑换成功对话框
		if MyCmpColor(455,213,"C2D6E2",30000)==0 then
			ndlog("等待兑换成功对话框失败")
			break;
		end
		
		--点击确定
		dm:MoveTo(550,442)
		dm:LeftClick()
		
		--判断兑换的金币和铜币数量是否正确
		local nJinbiMinus=nJinbi-获取玩家金币()		--扣除的金币值
		if nJinbiMinus~=nValue then
			ndlog(string.format("【测试铜钱兑换】金币扣除异常，输入值/当前扣除%d/%d",nValue,nJinbiMinus))
			break;
		end
		
		local nTongbiAppend=获取玩家铜币()-nTongbi		--增加的铜币值
		local nTongbiNeedAppend=nValue*100			--需要增加的铜币值
		if nTongbiAppend~=nTongbiNeedAppend then
			ndlog(string.format("【测试铜钱兑换】铜币增加异常，需要增加/当前增加%d/%d",nTongbiNeedAppend,nTongbiAppend))
			break;
		end
		
		dm:Delay(1000)
		
	end
	
	ndlog("【测试铜钱兑换】测试结束")

end

铜币兑换测试(100)