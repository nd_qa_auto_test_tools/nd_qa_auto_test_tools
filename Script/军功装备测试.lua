
--[[***********军功装备解锁购买测试***************
                   _ooOoo_
                  o8888888o
                  88" . "88
                  (| -_- |)
                  O\  =  /O
               ____/`---'\____
             .'  \\|     |//  `.
            /  \\|||  :  |||//  \
           /  _||||| -:- |||||-  \
           |   | \\\  -  /// |   |
           | \_|  ''\---/''  |   |
           \  .-\__  `-`  ___/-. /
         ___`. .'  /--.--\  `. . __
      ."" '<  `.___\_<|>_/___.'  >'"".
     | | :  `- \`.;`\ _ /`;.`/ - ` : | |
     \  \ `-.   \_ __\ /__ _/   .-` /  /
======`-.____`-.___\_____/___.-`____.-'======
                   `=---='
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         佛祖保佑       永无BUG
]]


local 总解锁数量=0
local 总购买装备数量=0

------------------------------------------------------------------------------------
-----------------------------------界面数据层---------------------------------------
------------------------------------------------------------------------------------
local tMapInfo = {}
	tMapInfo["LeftSpacing"] = 140 --第一列控件与左边菜单的间距
	tMapInfo["TopSpacing"] = 8 --第一排控件与上边菜单的间距
	tMapInfo["LineSpacing"] = -1 --各行之间的间距
	tMapInfo["ListSpacing"] = 20 --各列之间的间距
	tMapInfo["ElementWidth"] = 127 --每个子元素的宽度
	tMapInfo["ElementHeight"] = 122 --每个子元素的高度
	tMapInfo["ElementLineCount"] = 6 --总共的行数
	tMapInfo["ElementListCount"] = 12 --总共的列数
	tMapInfo["FrameTier"] = "13" --底图的层级
	tMapInfo["ButtonTier"] = "14" --底图的层级
	tMapInfo["LineTier"] = "15" --画线的层级
	tMapInfo["ButtonWidth"] = 89 --按钮的宽
	tMapInfo["ButtonHeight"] = 25 --按钮的高
	tMapInfo["SoliderId"] = 999 --军功装备默认官阶ID
	tMapInfo["ItemFrame"] = "Explotit_Item_" --物品控件名
	tMapInfo["ItemFrameBtn"] = "Explotit_ItemBtn_" --按钮控件名
	tMapInfo["ItemFrameLine"] = "Explotit_ItemLine_" --画线名
	tMapInfo["ItemFrameArrows"] = "Explotit_ItemArrows_" --箭头名
	
	tMapInfo["Used"] = false --动态表是否创建的标记，false：创建了未加入数据,true：创建并加入数据
local tLayoutInfo = {} --界面布局表。每一个位置都指定了在界面上的坐标。
local tEquipData = {} --科技详细数据
local tDeleteFrameData = {} --界面清空容器数据

local GetTechTreeLayoutDataScript=[[
return (function()
	local szData=""
	local tData=GetTechTreeLayoutData_lua(%d,%d) or {}
	szData=table.serialize(tData)
	return szData
end)()
]]

--动态创建布局界面数据
local function CreateLayout()
	tLayoutInfo = {}
	for nLine = 0,tMapInfo["ElementLineCount"] - 1 do 
		tLayoutInfo[nLine] = tLayoutInfo[nLine] or {}
		for nList = 1,tMapInfo["ElementListCount"] do
			tLayoutInfo[nLine][nList] = tLayoutInfo[nLine][nList] or {}
			tLayoutInfo[nLine][nList]["CardId"] = 0 --保存技能ID
			tLayoutInfo[nLine][nList]["vecPrepCardIds"] = {} --保存前置技能ID
			tLayoutInfo[nLine][nList]["vecBackCardIds"] = {} --保存后续技能ID
			--子元素在界面的位置
			tLayoutInfo[nLine][nList]["Left"] = tMapInfo["LeftSpacing"]+(tMapInfo["ElementWidth"]+tMapInfo["ListSpacing"])*(nList-1)
			tLayoutInfo[nLine][nList]["Top"] = tMapInfo["TopSpacing"]+(tMapInfo["ElementHeight"]+tMapInfo["LineSpacing"])*nLine
			tLayoutInfo[nLine][nList]["Right"] = tLayoutInfo[nLine][nList]["Left"]+tMapInfo["ElementWidth"]
			tLayoutInfo[nLine][nList]["Bottom"] = tLayoutInfo[nLine][nList]["Top"]+tMapInfo["ElementHeight"]
		end
	end
end

--获取购买装备需要的铜币价格
local function GetCostMoney(nItemId)
	local strScript=[[return (function()
		local nMoney = 0
		local nScienceId = GetScienceId_lua(nItemId)
		if nil ~= GetDataFromIni_lua("itemtype",nScienceId) then --获取科技价格
			nMoney = GetDataFromIni_lua("itemtype",nScienceId).money or 0
		end
		return nMoney
	end)()]]
	local n=nil
	repeat
		n=dm:ExecuteGameLuaRet(string.format(strScript,nItemId))
	until n~=nil
	return n
end

--取解锁装备需要的功勋
local function GetCostExploit(nItemId)
	local strScript=[[return (function()
		local nExploit = 0
		local nItemId=%d
		if nil ~= GetDataFromIni_lua("science_tree",nItemId) then --获取科技经验
			nExploit = GetDataFromIni_lua("science_tree",nItemId)["exploit"] or 0
		end
		return nExploit
	end)()]]
	local n=nil
	repeat
		n=dm:ExecuteGameLuaRet(string.format(strScript,nItemId))
	until n~=nil
	return n

	
end

--读取现有数据将数据存入布局界面数据表
local function ExplotitEquipment_CreateEquipData() 
	local szData=nil
	repeat
		szData=dm:ExecuteGameLuaRet(string.format(GetTechTreeLayoutDataScript,2,tMapInfo["SoliderId"]))
	until szData~=nil
	
	local tEquipData = loadstring(szData)()

	CreateLayout()
	
	for nCardId,tValue in pairs(tEquipData) do 
		local nIndex_List = tValue["nPosX"] or 0
		local nIndex_Line = tValue["nPosY"] or 0
		local tPrepCardIds = tValue["vecPrepCardIds"] or {}
		local tBackCardIds = tValue["vecBackCardIds"] or {}
		-- 判断输入的位置不为空，且类型是一个表
		if tLayoutInfo[nIndex_Line][nIndex_List] ~= nil and type(tLayoutInfo[nIndex_Line][nIndex_List]) == "table" then
			tLayoutInfo[nIndex_Line][nIndex_List]["CardId"] = nCardId
			tLayoutInfo[nIndex_Line][nIndex_List]["vecPrepCardIds"] = tPrepCardIds --保存前置技能ID
			tLayoutInfo[nIndex_Line][nIndex_List]["vecBackCardIds"] = tBackCardIds --保存后续技能ID
		end
	end
end

--调用游戏接口购买装备
local function BuyEquip(nItemId,nLine,nList)
	local strScript=[[(function()
		local nItemId=%d
		local nScienceTypeId = GetScienceTypeId_lua(nItemId)
		local nScienceId = GetScienceId_lua(nItemId)
		TechTreePlayerBuyScience_lua(nScienceTypeId,nScienceId)
		TK_FrameShow("NormalWaiting")
	end)()
	]]
	local nCopper=获取玩家铜币()
	local nNeedCopper=GetCostMoney(nItemId)
	dm:ExecuteGameLua(string.format(strScript,nItemId))
	Util.WaitNormalWaiting()
	总购买装备数量=总购买装备数量+1
	ndlog("【军功装备购买】行/列:%d/%d,科技ID:%d,铜币/消耗:%d/%d",nLine+1,nList,nItemId,nNeedCopper,nCopper-获取玩家铜币())
end

--解锁科技
local function UnlockScience(nItemId)
	local strScript=[[(function()
		local nItemId=%d 
		local nScienceTypeId = GetScienceTypeId_lua(nItemId)
		TechTreeUnlockScience_lua(nItemId,nScienceTypeId)
		TK_FrameShow("NormalWaiting")
	end)()
	]]
	dm:ExecuteGameLua(string.format(strScript,nItemId))
end

--是否可以解锁
local function IsItemCanUnlock(nItemId)
	local strScript=[[return (function()
		local nRet=0
		if GetScienceUnlocked_lua(%d) then
			nRet=0
		else 
			nRet=1
		end
		return nRet
	end)()]]
	local n=nil
	repeat
		n=dm:ExecuteGameLuaRet(string.format(strScript,nItemId))
	until n~=nil
	return n==1
	
end

--装备是否可以购买给主角
local function IsEquipCanBuy(nItemId)
	local strScript=[[return (function()
		local nRet=0
		local nItemId=%d
		if GetScienceUnlocked_lua(nItemId) then
			if GetSciencePlayerBuyed_lua(nItemId) then
				nRet=0
			else
				nRet=1
			end
		end
		
		return nRet
	end)()
	]]
	
	local nCanBuy=nil
	repeat
		nCanBuy=dm:ExecuteGameLuaRet(string.format(strScript,nItemId))
	until nCanBuy~=nil
	return nCanBuy==1
end

local function 军功装备测试()

	ndlog("------------------【军功装备】测试开始------------------")
	
	ExplotitEquipment_CreateEquipData()

	--行,列
	local nLine,nList=0,1
	while nLine < tMapInfo["ElementLineCount"] do 
		while nList <= tMapInfo["ElementListCount"] do
		
			local nCardId=tLayoutInfo[nLine][nList]["CardId"]
			
			repeat
				if nCardId==0 then break end
				local nCopper=获取玩家铜币()
				
				
				if IsItemCanUnlock(nCardId) then
					local nExplotit=获取玩家军功()
					local nNeedExplotit=GetCostExploit(nCardId)
					UnlockScience(nCardId)
					Util.WaitNormalWaiting()
					总解锁数量=总解锁数量+1
					local szItemName = 获取物品名(nCardId)
					ndlog("【军功装备解锁】行/列:%d/%d,科技:%s,军功/消耗:%d/%d",nLine+1,nList,szItemName,nNeedExplotit,nExplotit-获取玩家军功())
					BuyEquip(nCardId,nLine,nList)
				elseif IsEquipCanBuy(nCardId) then
					BuyEquip(nCardId,nLine,nList)
					
				end
			
			until true;
			
			nList=nList+1
				

		end
		nList=1
		nLine=nLine+1
	end
	ndlog("------------------【军功装备】测试结束------------------")
end


while true do
	
	--如果科技树升级界面没有显示，那么打开
	if not GameFrameIsVisible("ExplotitEquipment") then
		-- 移动到升级按钮并点击
		dm:MoveTo(491,22)
		dm:LeftClick()

		-- 判断窗口是否打开
		if MyCmpColor(1244,101,"d9d1ba",3000)==0 then
			ndlog("打开升级窗口失败")
			break;
		end
		Util.WaitNormalWaiting()
		if dm:CmpColor(421,107,"708db4",0.9)~=0 then
			dm:MoveTo(382,106)
			dm:LeftClick()
			Util.WaitNormalWaiting()
		end
		dm:Delay(1000)
		
	end
	
	local 玩家铜币=获取玩家铜币()
	local 玩家军功=获取玩家军功()
	

	军功装备测试()
	
	local 军功消耗=玩家军功-获取玩家军功()
	local 铜币消耗=玩家铜币-获取玩家铜币()
	
	ndlog("军功/消耗:%d/%d,铜币/消耗:%d/%d",玩家军功,军功消耗,玩家铜币,铜币消耗)
	
	ndlog("军功装备测试全部完成,总解锁数量:%d,总购买装备数量:%d",总解锁数量,总购买装备数量)

	break;
end








