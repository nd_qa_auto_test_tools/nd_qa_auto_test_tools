

local function 副将招募测试()
	
	-- 绑定窗口
	local bSucess=dm:BindWindow(hHubaoqiFrontendWnd,"dx", "dx", "dx", 0)
	if bSucess == 0 then
		ndlog("绑定游戏窗口失败")
		return
	end

	--移动到副将按钮并点击
	dm:MoveTo(428,19)
	dm:LeftClick()
	
	-- 判断窗口是否打开
	if MyCmpColor(34,105,"7997bf",3000)==0 then
		ndlog("打开副将窗口失败")
		return;
	end
	
	dm:Delay(2000)
	
	local 招募数=13
	
	--y坐标起点，步长98
	local StartPointY=65
	local PointX,PointY=1233,163
	
	--第一个副将默认出战了，从第二个副将开始
	for nCount=2,招募数 do
	
		ndlog("【副将招募测试】正在进行%d/%d次招募",nCount,招募数)
		
		local nRow=nCount % 6
		
		--判断是否该翻页了
		if nRow==1 then
			
			--点击下一页
			dm:MoveTo(1153,725)
			dm:LeftClick()
			
			--等待1秒
			dm:Delay(1000)
		elseif nRow==0 then
			nRow=6
		end
		
		--计算y坐标
		PointY=StartPointY+(nRow)*98
		
		--点击副将
		dm:MoveTo(PointX,PointY)
		dm:LeftClick()
		
		--等待1秒
		dm:Delay(1000)
		
		--点击招募按钮
		dm:MoveTo(844,154)
		dm:LeftClick()
		
		-- 等待确认招募副将窗口打开
		if MyCmpColor(454,212,"c2d6e2",3000)==0 then
			ndlog("等待确认招募副将窗口打开失败")
			break;
		end
		
		--点击确定按钮
		dm:MoveTo(549,439)
		dm:LeftClick()
		
		--等待1秒
		-- dm:Delay(1000)
		
		
		
	end
	
	ndlog("【副将招募测试】测试结束")
	dm:UnBindWindow()
end

local function 副将出战测试()
	
	--如果副将按钮正常颜色，说明没打开
	if MyCmpColor(419,20,"d9d1ba",100)==1 then

		--移动到副将按钮并点击
		dm:MoveTo(419,20)
		dm:LeftClick()
		
		-- 判断窗口是否打开
		if MyCmpColor(34,105,"7997bf",3000)==0 then
			ndlog("打开副将窗口失败")
			return;
		end
	
	end
	
	dm:ExecuteGameLua("PushButton_LieutenantInfo_List_Left() PushButton_LieutenantInfo_List_Left() PushButton_LieutenantInfo_List_Left()")
	dm:Delay(2000)
	
	local 出战数=13
	
	--y坐标起点，步长98
	local StartPointY=65
	local PointX,PointY=1233,163
	
	for nCount=1,出战数 do
	
		
		
		local nRow=nCount % 6
		
		--判断是否该翻页了
		if nCount~=1 and nRow==1 then
			
			--点击下一页
			dm:MoveTo(1153,725)
			dm:LeftClick()
			
			--等待1秒
			dm:Delay(1000)
		elseif nRow==0 then
			nRow=6
		end
		
		--计算y坐标
		PointY=StartPointY+(nRow)*98
		
		--点击副将
		dm:MoveTo(PointX,PointY)
		dm:LeftClick()
		
		--等待1秒
		dm:Delay(1000)
		
		--点击出战按钮
		dm:MoveTo(844,154)
		dm:LeftClick()
		
		-- 等待出战成功
		if MyCmpColor(PointX,PointY,"f0dfdd",3000)==0 then
			ndlog("等待出战成功失败")
			break;
		end
		
		--打印出战成功的副将名
		local 点击副将名 = dm:ExecuteGameLuaRet(string.format('local strRet=GetProperty_lua("TextFrame_Lieutenant_List_No%d","Normal_10_Text") return strRet',nCount))
		local 出战副将名 = dm:ExecuteGameLuaRet('local strRet=GetProperty_lua("PushButton_LieutenantInfo_Open","Normal_2_Text") return strRet')
		ndlog("【副将出战测试】%d/%d次出战,点击/出战：%s/%s", nCount, 出战数, 点击副将名, 出战副将名)
		
		--判断点击出战的副将名字是否和出战的副将名字一样
		if string.find(出战副将名,点击副将名) == nil then
			ndlog("副将实际出战与点击的副将不符")
			break;
		end
		
		--等待1秒
		-- dm:Delay(1000)

	end
	
	ndlog("【副将出战测试】测试结束")
end

-- 副将招募测试()

副将出战测试()






