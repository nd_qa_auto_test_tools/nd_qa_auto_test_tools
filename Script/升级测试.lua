
--[[***********兵种科技树升级测试***************
                   _ooOoo_
                  o8888888o
                  88" . "88
                  (| -_- |)
                  O\  =  /O
               ____/`---'\____
             .'  \\|     |//  `.
            /  \\|||  :  |||//  \
           /  _||||| -:- |||||-  \
           |   | \\\  -  /// |   |
           | \_|  ''\---/''  |   |
           \  .-\__  `-`  ___/-. /
         ___`. .'  /--.--\  `. . __
      ."" '<  `.___\_<|>_/___.'  >'"".
     | | :  `- \`.;`\ _ /`;.`/ - ` : | |
     \  \ `-.   \_ __\ /__ _/   .-` /  /
======`-.____`-.___\_____/___.-`____.-'======
                   `=---='
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         佛祖保佑       永无BUG
]]

-- 类型ID，名称，按钮名
local tContryConfig={
	{1,"魏","PushBtn_TechTree_CountryWei"},
	{2,"蜀","PushBtn_TechTree_CountryShu"},
	{3,"吴","PushBtn_TechTree_CountryWu"},
	{5,"罗马","PushBtn_TechTree_CountryRoman"},
	{8,"特殊","PushBtn_TechTree_CountrySupper"}
}

local 总解锁数量=0
local 总购买士兵数量=0
local 总购买装备数量=0

--拖拽滚动条
local function DragScrollbar()
	dm:ExecuteGameLua(
		[[
		(function()	
			local scroll_position = ToScrollView(GetWindowObj_lua("ScrollView_TechTreeDetail_Equipment")):GetVerticalScrollPosition() 
			ToScrollView(GetWindowObj_lua("ScrollView_TechTreeDetail_Equipment")):SetVerticalScrollPosition(scroll_position + 100)
		)()
		]])
end

------------------------------------------------------------------------------------
-----------------------------------界面数据层---------------------------------------
------------------------------------------------------------------------------------
local tMapIndex = {}
	tMapIndex["LeftSpacing"] = 123 --第一列控件与左边菜单的间距
	tMapIndex["TopSpacing"] = 5 --第一排控件与上边菜单的间距
	tMapIndex["LineSpacing"] = 55 --各行之间的间距
	tMapIndex["ListSpacing"] = 13 --各列之间的间距
	tMapIndex["ElementWidth"] = 103 --每个子元素的宽度
	tMapIndex["ElementHeight"] = 156 --每个子元素的高度
	tMapIndex["ElementLineCount"] = 5 --总共的行数,只能变小，不能变大，该值大于5，界面无法识别
	tMapIndex["ElementListCount"] = 12 --总共的列数,只能变小，不能变大，该值大于12，界面无法识别
	tMapIndex["BackgroundTier"] = "12" --底图的层级
	tMapIndex["ButtonTier"] = "13" --底图的层级
	tMapIndex["ButtonWidth"] = 89 --tMapIndex["ElementWidth"] --按钮的宽
	tMapIndex["ButtonHeight"] = 25 --按钮的高
	tMapIndex["LineTier"] = "14" --连线的层级
	tMapIndex["Used"] = false --动态表是否创建的标记，false：创建了未加入数据,true：创建并加入数据
local tLayoutInfo = {} --界面布局表。每一个位置都指定了在界面上的坐标。

local tMapInfo = {}
	tMapInfo["LeftSpacing"] = 410 --第一列控件与左边菜单的间距
	tMapInfo["TopSpacing"] = 8 --第一排控件与上边菜单的间距
	tMapInfo["LineSpacing"] = -1 --各行之间的间距
	tMapInfo["ListSpacing"] = 17 --各列之间的间距
	tMapInfo["ElementWidth"] = 127 --每个子元素的宽度
	tMapInfo["ElementHeight"] = 122 --每个子元素的高度
	tMapInfo["ElementLineCount"] = 12 --总共的行数
	tMapInfo["ElementListCount"] = 7 --总共的列数
	tMapInfo["FrameTier"] = "13" --底图的层级
	tMapInfo["ButtonTier"] = "14" --底图的层级
	tMapInfo["LineTier"] = "15" --画线的层级
	tMapInfo["ButtonWidth"] = 89 --按钮的宽
	tMapInfo["ButtonHeight"] = 25 --按钮的高
	tMapInfo["Used"] = false --动态表是否创建的标记，false：创建了未加入数据,true：创建并加入数据
local tDetailLayoutInfo = {} --界面布局表。每一个位置都指定了在界面上的坐标。
local nCardMaxNum = 6 --该界面的最高列数，用来区别技能与兵牌

local GetTechTreeLayoutDataScript=[[
return (function()
	local szData=""
	local tData=GetTechTreeLayoutData_lua(%d,%d) or {}

	szData=table.serialize(tData)
	return szData
end)()
]]

--动态创建布局界面数据
local function CreateLayout()
	if tMapIndex["Used"] then
		return 
	end
	tLayoutInfo = {}
	tMapIndex["Used"] = false --创建动态表标记为未使用
	for nLine = 0,tMapIndex["ElementLineCount"] - 1 do 
		tLayoutInfo[nLine] = tLayoutInfo[nLine] or {}
		for nList = 0,tMapIndex["ElementListCount"] - 1 do
			tLayoutInfo[nLine][nList] = tLayoutInfo[nLine][nList] or {}
			
			tLayoutInfo[nLine][nList]["Sodier"] = 0 --保存技能ID
			tLayoutInfo[nLine][nList]["BeforeSodier"] = {} --保存前置技能ID
			tLayoutInfo[nLine][nList]["AfterSodier"] = {} --保存后续技能ID
			--子元素在界面的位置
			tLayoutInfo[nLine][nList]["Left"] = tMapIndex["LeftSpacing"]+(tMapIndex["ElementWidth"]+tMapIndex["ListSpacing"])*nList
			tLayoutInfo[nLine][nList]["Top"] = tMapIndex["TopSpacing"]+(tMapIndex["ElementHeight"]+tMapIndex["LineSpacing"])*nLine
			tLayoutInfo[nLine][nList]["Right"] = tLayoutInfo[nLine][nList]["Left"]+tMapIndex["ElementWidth"]
			tLayoutInfo[nLine][nList]["Bottom"] = tLayoutInfo[nLine][nList]["Top"]+tMapIndex["ElementHeight"]
		end
	end
end

--动态创建布局界面数据
local function TechTree_SodierDetail_CreateLayout()

	if tMapInfo["Used"] then
		return 
	end
	
	tDetailLayoutInfo = {}
	tMapInfo["Used"] = false --创建动态表标记为未使用
	for nLine = 0,tMapInfo["ElementLineCount"] - 1 do 
		tDetailLayoutInfo[nLine] = tDetailLayoutInfo[nLine] or {}
		for nList = 1,tMapInfo["ElementListCount"] do
			tDetailLayoutInfo[nLine][nList] = tDetailLayoutInfo[nLine][nList] or {}
			tDetailLayoutInfo[nLine][nList]["CardId"] = 0 --保存技能ID
			tDetailLayoutInfo[nLine][nList]["vecPrepCardIds"] = {} --保存前置技能ID
			tDetailLayoutInfo[nLine][nList]["vecBackCardIds"] = {} --保存后续技能ID
			--子元素在界面的位置
			tDetailLayoutInfo[nLine][nList]["Left"] = tMapInfo["LeftSpacing"]+(tMapInfo["ElementWidth"]+tMapInfo["ListSpacing"])*(nList-1)
			tDetailLayoutInfo[nLine][nList]["Top"] = tMapInfo["TopSpacing"]+(tMapInfo["ElementHeight"]+tMapInfo["LineSpacing"])*nLine
			tDetailLayoutInfo[nLine][nList]["Right"] = tDetailLayoutInfo[nLine][nList]["Left"]+tMapInfo["ElementWidth"]
			tDetailLayoutInfo[nLine][nList]["Bottom"] = tDetailLayoutInfo[nLine][nList]["Top"]+tMapInfo["ElementHeight"]
		end
	end
end

--检测是否是金币兵种
local function TechTreeSodierIsGold(nRankId)
	--金币兵种段
	if nRankId >= 800 and nRankId <= 899 then
		return true
	end
	return false
end

local function GetSodierTypeId(nRankId)
	local szScript=[[return (function()
		local nSodierId = 0
		nSodierId = GetDataFromIni_lua("rank_tree",%d).army_type or 0
		return nSodierId
	end)()
	]]
	return dm:ExecuteGameLuaRet(string.format(szScript,nRankId))
end
 
--获取官阶购买需要的金币
local function GetCostMoney(nItemId)
	local szScript=[[return (function()
		local nMoney = 0
		local nItemId=%d
		if nItemId < 1000 then -- 兵牌
			local nSodierId = GetDataFromIni_lua("rank_tree",nItemId).army_type or 0
			if nil ~= GetDataFromIni_lua("armytype",nSodierId) then --获取士兵价格
				nMoney = GetDataFromIni_lua("armytype",nSodierId).establish_price or 0
			end
		else --科技
			local nScienceId = GetScienceId_lua(nItemId)
			if nScienceId > 10000 then
				if nil ~= GetDataFromIni_lua("itemtype",nScienceId) then --获取科技价格
					nMoney = GetDataFromIni_lua("itemtype",nScienceId).money or 0
				end
			end
		end
		return nMoney
	end)()]]
	local n=nil
	repeat
		n=dm:ExecuteGameLuaRet(string.format(szScript,nItemId))
	until n~=nil
	return n
end

--获取升级消耗的经验
local function GetCostExp(nItemId)
	local szScript=[[
		local nExp = 999999999
		local nItemId=%d
		
		local tIniParam = 
		{
			RankConNum = 3,--官阶条件个数
			SciConNum = 5
		}

		if nItemId < 1000 then --官阶ID
			local tRecord = GetDataFromIni_lua("rank_tree",nItemId)
			if nil~= tRecord then
				for i=2,tIniParam.RankConNum do 
					if nil ~= tRecord["pre_rank" .. i] then
						nExp = tRecord["pre_rank1_exp"]
						if nExp < tRecord["pre_rank"..i.."_exp"] then
							nExp = tRecord["pre_rank"..i.."_exp"]
							-- break
						end
					end
				end
			end
		else
			if nil ~= GetDataFromIni_lua("science_tree",nItemId) then --获取科技经验
				nExp = GetDataFromIni_lua("science_tree",nItemId)["exp"]
			end
		end
		return nExp
	]]
	return dm:ExecuteGameLuaRet(string.format(szScript,nItemId))
end

--获取士兵布局
local function ConfigTeachTreeSoldierLayout(nCountryID)

	local szData=nil
	repeat
		szData=dm:ExecuteGameLuaRet(string.format(GetTechTreeLayoutDataScript,1,nCountryID))
	until szData~=nil
	
	local tData = loadstring(szData)()

	CreateLayout()
	
	--将传进来的数据按照对应的坐标值，输入进动态布局表中
	for nRankId,tValue in pairs(tData) do 
		local nIndex_List = tValue["nPosX"] or 0
		local nIndex_Line = tValue["nPosY"] or 0
		local tBeforeSodier = tValue["vecPrepCardIds"] or {}
		local tfterSodier = tValue["vecBackCardIds"] or {}
		
		-- 判断输入的位置不为空，且类型是一个表
		if tLayoutInfo[nIndex_Line][nIndex_List] ~= nil and type(tLayoutInfo[nIndex_Line][nIndex_List]) == "table" then
			tLayoutInfo[nIndex_Line][nIndex_List]["Sodier"] = nRankId
			tLayoutInfo[nIndex_Line][nIndex_List]["BeforeSodier"] = tBeforeSodier --保存前置技能ID
			tLayoutInfo[nIndex_Line][nIndex_List]["AfterSodier"] = tfterSodier --保存后续技能ID
		end
	end

end

--获取官阶对应的详细布局
--@param nRankID 官阶id
local function ConfigTeachTreeSoldierDetail(nRankID)
	
	local szData=nil
	repeat
		szData=dm:ExecuteGameLuaRet(string.format(GetTechTreeLayoutDataScript,2,nRankID))
	until szData~=nil
	
	local tEquipData=loadstring(szData)()
	tMapInfo["Used"] = false
	TechTree_SodierDetail_CreateLayout()
	--将传进来的数据按照对应的坐标值，输入进动态布局表中
	for nCardId,tValue in pairs(tEquipData) do 
		local nIndex_List = tValue["nPosX"] or 0
		local nIndex_Line = tValue["nPosY"] or 0
		local tPrepCardIds = tValue["vecPrepCardIds"] or {}
		local tBackCardIds = tValue["vecBackCardIds"] or {}
		
		-- 判断输入的位置不为空，且类型是一个表
		if tDetailLayoutInfo[nIndex_Line][nIndex_List] ~= nil and type(tDetailLayoutInfo[nIndex_Line][nIndex_List]) == "table" then
			tDetailLayoutInfo[nIndex_Line][nIndex_List]["CardId"] = nCardId
			tDetailLayoutInfo[nIndex_Line][nIndex_List]["vecPrepCardIds"] = tPrepCardIds --保存前置技能ID
			tDetailLayoutInfo[nIndex_Line][nIndex_List]["vecBackCardIds"] = tBackCardIds --保存后续技能ID
			
			-- if nIndex_List > nCardMaxNum then
				-- nCardMaxNum = nIndex_List
			-- end
		end
	end
	tMapInfo["Used"] = true --创建动态表标记为使用中
	
end

--士兵是否能购买
local function IsSoldierCanBuy(nRankId)
	local szScript=[[return (function()
		if TechTree_CardStatus_BuyAlready(%d) then
			return 0
		end
		
		if TechTree_CardStatus_StatusCanBuy(%d) then
			return 1 
		else 
			return 0 
		end
	end)()
	]]
	local n=nil
	repeat
		n=dm:ExecuteGameLuaRet(string.format(szScript,nRankId,nRankId))
	until n~=nil
	return n==1
end

local function TechTreeSodierIsGoldGetGold(nRankId)
	local szScript=[[return (function()
		local nPrice=0
		local nRankId=%d
		local tSodierTable = GetDataFromIni_lua("rank_tree",nRankId)
		nPrice = tSodierTable.pre_rank1_price or 0
		lua_log_debug("sddddddddddddddddddd-"..tostring(nPrice))
		return nPrice
	end)()
	]]
	local n=nil
	repeat
		n=dm:ExecuteGameLuaRet(string.format(szScript,nRankId))
	until n~=nil
	return n
end

local function TechTreeCardStatusBuyAlready(nRankId)
	local szScript=[[return (function()
		if TechTree_CardStatus_BuyAlready(%d) then
			return 1
		else
			return 0
		end
	end)()
	]]
	local n=nil
	repeat
		n=dm:ExecuteGameLuaRet(string.format(szScript,nRankId))
	until n~=nil
	return n==1
end

--调用游戏接口购买装备
--@param nTarget 1自己，2士兵
local function BuyEquip(nItemId,nTarget)
	local szScript=[[(function()
		local nItemId=%d
		local nTarget=%d
		if nTarget==1 then
			TechTreePlayerBuyScience_lua(GetScienceTypeId_lua(nItemId),GetScienceId_lua(nItemId))
		else
			TechTreeArmyBuyScience_lua(nItemId,GetScienceId_lua(nItemId))
		end
		TK_FrameShow("NormalWaiting")
	end)()
	]]
	dm:ExecuteGameLua(string.format(szScript,nItemId,nTarget))
end

--解锁科技
local function UnlockScience(nItemId,nRankId)
	local szScript=[[(function()
		local nItemId=%d 
		local nRankId=%d 
		if nItemId < 1000 then --官阶
			TechTreeUnlockSodier_lua(nItemId,nRankId)
			TK_FrameShow("NormalWaiting")
		else
		
			local nScienceTypeId = GetScienceTypeId_lua(nItemId)
			local nScienceId = GetScienceId_lua(nItemId)

			NormalMsgWindow_Close()
			TechTreeUnlockScience_lua(nItemId,nScienceTypeId)
			TK_FrameShow("NormalWaiting")
		end
	end)()
	]]
	dm:ExecuteGameLua(string.format(szScript,nItemId,nRankId))
end

--是否可以解锁
local function IsItemCanUnlock(nItemId)
	local szScript=[[return (function()
		if not TechTree_TechStatus_StatusCanBuy(%d) then
			return 1 
		else 
			return 0 
		end
	end)()]]
	local n=nil
	repeat
		n=dm:ExecuteGameLuaRet(string.format(szScript,nItemId))
	until n~=nil
	return n==1
	
end

--装备是否可以购买给主角或者士兵
--@param nRankId 当前官阶ID，判断士兵是否招募用的
--@param nItemId 当前要购买的物品id
local function IsEquipCanBuy(nRankId,nItemId)
	local szScript=[[return (function()
		local szRet="11"
		local nItemId=%d
		local nRankId=%d
		local bCon1,bCon2=true,true
		local nType = TechTree_SodierDetail_GetItemType(nItemId)
		if nType==1 then
			while true do
			
				if not TechTree_TechStatus_Unlocked(nItemId) then
					bCon1=false
					break;
				end
				
				if not TechTree_TechStatus_StatusCanBuy(nItemId) then --还没有研发，返回false
					bCon1=false
					break;
				end
				
				if GetSciencePlayerBuyed_lua(nItemId) then --已经购买了，返回false
					bCon1=false
					break;
				end
				
				break;
			end
			
			while true do
				
				if not TechTree_TechStatus_Unlocked(nItemId) then --当前兵牌没解锁，返回false
					bCon2=false
					break;
				end
				
				if not TechTree_CardStatus_BuyAlready(nRankId) then	--当前官阶未购买，返回false
					bCon2=false
					break;
				end
				
				if not TechTree_TechStatus_StatusCanBuy(nItemId) then --还没有研发，返回false
					bCon2=false
					break;
				end
				
				if GetScienceArmyBuyed_lua(nItemId) then
					bCon2=false
					break;
				end
				
				if ChkArmyEquip_lua(nItemId) then
					bCon2=false
					break;
				end
				
				break;
			end
		else
			bCon1=false
			bCon2=false
		end
		if bCon1 then
			szRet="1"
		else
			szRet="0"
		end
		if bCon2 then
			szRet=szRet.."1"
		else
			szRet=szRet.."0"
		end
		lua_log_debug("-----------------------------------------------")
		lua_log_debug(szRet)
		return szRet
	end)()
	]]
	
	local szCanBuy=nil
	repeat
		szCanBuy=dm:ExecuteGameLuaRet(string.format(szScript,nItemId,nRankId))
	until szCanBuy~=nil
	if szCanBuy=="11" then
		return true,true
	elseif szCanBuy=="10" then
		return true,false
	elseif szCanBuy=="01" then
		return false,true
	else
		return false,false
	end 
end

--找图并且移动鼠标到图片
local function FindPicAndMoveTo(PicFile)
	local bFind,x,y=dm:FindPic(160,100,nScreenWidth,nScreenHeight,GetPicturePath(PicFile),"000000",0.9,0);
	if bFind~=-1 then
		dm:MoveTo(x,y)
		return true
	end
	return false
end

local function WaitTechTree_BuyEffect()
	local timeout = 20000		--超时20秒
	while GameFrameIsVisible("TechTree_BuyEffect") do
		dm:Delay(100)
		timeout=timeout-100
		if timeout<=0 then
			return false;
		end
	end
	return true;
end


--选择国家
local function ChooseContry(CountryName)
	dm:ExecuteGameLua(string.format('Event_BtnClick_TechTree_SelectedCountry("%s")',CountryName))
end

local function 处理解锁(nRankId)

	--初始化官阶详细装备信息
	ConfigTeachTreeSoldierDetail(nRankId)
	
	--行,列
	local nLine,nList=0,1
	while nLine < tMapInfo["ElementLineCount"] do 
		while nList <= tMapInfo["ElementListCount"] do
			local nCardId=tDetailLayoutInfo[nLine][nList]["CardId"]
			
			repeat
				if nCardId==0 then break end
				
				if IsItemCanUnlock(nCardId) then
					local nNeedGeneralExp=GetCostExp(nCardId)
					local nGeneralExp=获取玩家声望()
					UnlockScience(nCardId,nRankId)
					Util.WaitNormalWaiting()
					总解锁数量=总解锁数量+1
					local szScienceName = ""
					if nCardId < 1000 then
						szScienceName = 获取士兵名(nCardId)
					else
						szScienceName = 获取物品名(nCardId)
					end
					ndlog("【处理解锁】行/列:%d/%d,科技/ID:%s/%d,声望/消耗:%s/%d",nLine+1,nList,szScienceName,nCardId,""..nNeedGeneralExp,nGeneralExp-获取玩家声望())
				end
				
			until true;
			
			nList=nList+1
		end
		nList=1
		nLine=nLine+1
	end
end

local function 装备购买(nRankId)

	--初始化官阶详细装备信息
	ConfigTeachTreeSoldierDetail(nRankId)
	
	--行,列
	local nLine,nList=0,1
	while nLine < tMapInfo["ElementLineCount"] do 
		while nList <= tMapInfo["ElementListCount"] do
			local nCardId=tDetailLayoutInfo[nLine][nList]["CardId"]
			
			repeat
				if nCardId==0 then break end
				
				if IsItemCanUnlock(nCardId) then
					UnlockScience(nCardId,nRankId)
					Util.WaitNormalWaiting()
				end
				
				--小于1000是士兵卡牌
				if nCardId < 1000 then break end

				--科技价格
				local nCardPrice=GetCostMoney(nCardId)
				if nCardPrice==0 then break end
				
				local nCopper=获取玩家铜币()
				
				local bSelfCanBuy,bSoldierCanBuy=IsEquipCanBuy(nRankId,nCardId)
				
				local nSelfExpend,nSoldierExpend=0,0
				if bSelfCanBuy then
					nSelfExpend=nCardPrice
					BuyEquip(nCardId,1)
					Util.WaitNormalWaiting()
					总购买装备数量=总购买装备数量+1
				end
				
				if bSoldierCanBuy then
					nSoldierExpend=nCardPrice
					BuyEquip(nCardId,2)
					Util.WaitNormalWaiting()
					总购买装备数量=总购买装备数量+1
				end
				if bSelfCanBuy or bSoldierCanBuy then
					local szItemName = 获取物品名(nCardId)
					ndlog("【装备购买】行/列:%d/%d,科技:%s,角色/士兵:%d/%d,消耗:%d",nLine+1,nList,szItemName,nSelfExpend,nSoldierExpend,nCopper-获取玩家铜币())
				end
				
			until true;
			
			nList=nList+1
		end
		nList=1
		nLine=nLine+1
	end
	
	
	
end

local function 士兵购买()

	ndlog("------------------【士兵购买】测试开始------------------")
	for i=1,#tContryConfig do
	
		ChooseContry(tContryConfig[i][3])
		Util.WaitNormalWaiting()
		ConfigTeachTreeSoldierLayout(tContryConfig[i][1])
		
		local nBuyCampSeatGold=0
		
		local nCopper=获取玩家铜币()
		local nGold=获取玩家金币()
		
		--行,列
		local nLine,nList=0,0
		while nLine < tMapIndex["ElementLineCount"] do 
			while nList < tMapIndex["ElementListCount"] do
			
				local nRankId=tLayoutInfo[nLine][nList]["Sodier"]
				local bCanBuy=nRankId~=0
				local bGoldSoldier=TechTreeSodierIsGold(nRankId)
				if bGoldSoldier then
					bCanBuy = bCanBuy and not TechTreeCardStatusBuyAlready(nRankId)
				else
					bCanBuy = bCanBuy and IsSoldierCanBuy(nRankId)
				end

				--未购买的士兵
				if bCanBuy then
					
					--购买按钮坐标
					local BuyBtnX,BuyBtnY=tLayoutInfo[nLine][nList]["Left"]+50,tLayoutInfo[nLine][nList]["Bottom"]+15+123

					dm:MoveTo(BuyBtnX,BuyBtnY)
					dm:LeftClick()
					-- 等待确认购买窗口打开
					local nRet=MyCmpColorWhich(460,219,"ecc47f",455,213,"c2d6e2",3000)
					
					--如果是购买兵营确认框
					if nRet==1 then
						
						nBuyCampSeatGold=500
						
						--点击确认按钮
						dm:MoveTo(549,440)
						dm:LeftClick()
						dm:Delay(1000)
						Util.WaitNormalWaiting()
					elseif nRet==2 then
						--点击确认按钮		
						dm:MoveTo(549,440)
						dm:LeftClick()
						
						总购买士兵数量=总购买士兵数量+1
						if bGoldSoldier then
							dm:Delay(500)
							WaitTechTree_BuyEffect()
						end
						Util.WaitNormalWaiting()
						ChooseContry(tContryConfig[i][3])
						Util.WaitNormalWaiting()
						
						local nDuductCopper=nCopper-获取玩家铜币()
						local nDuductGold=nGold-获取玩家金币()
						
						--士兵价格
						local nSoldierPrice=0
						if bGoldSoldier then
							nSoldierPrice=TechTreeSodierIsGoldGetGold(nRankId)
						else
							nSoldierPrice=GetCostMoney(nRankId)
						end
						
						local szSodierName=获取士兵名(nRankId)
						
						ndlog("【士兵购买】%s 士兵：%s 行/列：%d/%d 价格/扣铜币：%d/%d,兵营/扣金币：%d/%d",tContryConfig[i][2],szSodierName,nLine+1,nList+1,nSoldierPrice,nDuductCopper,nBuyCampSeatGold,nDuductGold)
						
						nList=nList+1
						nCopper=获取玩家铜币()
						nGold=获取玩家金币()
					end
					
					
				else
					nBuyCampSeatGold=0
					nList=nList+1
					
				end
			end
			nList=0
			nLine=nLine+1
		end
		
	end
	ndlog("------------------【士兵购买】测试结束------------------")
end

local function 升级测试(TestName,TestFunction)
	
	ndlog("------------------【%s】测试开始------------------",TestName)
	
	for i=1,#tContryConfig do
	
		--特殊兵种不需要解锁
		if TestName=="解锁测试" and tContryConfig[i][1]==8 then
			break;
		end
	
		ChooseContry(tContryConfig[i][3])
		ConfigTeachTreeSoldierLayout(tContryConfig[i][1])
		Util.WaitNormalWaiting()
		
		--行
		for nLine = 0,tMapIndex["ElementLineCount"] - 1 do 
			
			--列
			for nList = 0,tMapIndex["ElementListCount"] - 1 do
				local nRankId=tLayoutInfo[nLine][nList]["Sodier"]
				if nRankId~=0 then
					--士兵卡片坐标
					local SouliderCardX,SouliderCardY=tLayoutInfo[nLine][nList]["Left"]+50,tLayoutInfo[nLine][nList]["Top"]+50+123
					
					ndlog("【%s】%s 选择士兵ID：%d 行/列：%d/%d",TestName,tContryConfig[i][2],nRankId,nLine+1,nList+1)
					
					for n=1,2 do
						dm:MoveTo(SouliderCardX,SouliderCardY)
						dm:LeftClick()
						if MyCmpColor(24,510,"d9d1ba",3000)==1 then
							--等待loading转菊结束
							Util.WaitNormalWaiting()
							
							--执行测试函数
							TestFunction(nRankId)
							
							--本轮测试结束，选择当前国家进入士兵卡牌界面
							ChooseContry(tContryConfig[i][3])
							
							--等待loading转菊结束
							Util.WaitNormalWaiting()
							
							break;
						end
					end
				end
			end
		
		end
		
	end
	
	ndlog("------------------【%s】测试结束------------------",TestName)
	
end

while true do
	
	--如果科技树升级界面没有显示，那么打开
	if not GameFrameIsVisible("TechTree_Country") then
		-- 移动到升级按钮并点击
		dm:MoveTo(491,22)
		dm:LeftClick()

		-- 判断窗口是否打开
		if MyCmpColor(1244,101,"d9d1ba",3000)==0 then
			ndlog("打开升级窗口失败")
			break;
		end

		dm:Delay(1000)
		
	end
	
	local 玩家声望=获取玩家声望()
	local 玩家铜币=获取玩家铜币()
	local 玩家金币=获取玩家金币()
	
	升级测试("解锁测试",处理解锁)
	士兵购买()
	
	--如果不是进行市场测试，那么购买装备
	if not g_bMarketTest then
		升级测试("装备购买测试",装备购买)
	end
	
	local 声望消耗=玩家声望-获取玩家声望()
	local 铜币消耗=玩家铜币-获取玩家铜币()
	local 金币消耗=玩家金币-获取玩家金币()
	
	ndlog("声望/消耗:%d/%d,铜币/消耗:%d/%d,金币/消耗:%d/%d",玩家声望,声望消耗,玩家铜币,铜币消耗,玩家金币,金币消耗)
	
	ndlog("升级测试全部完成，总解锁数量:%d,总购买士兵数量:%d,总购买装备数量:%d",总解锁数量,总购买士兵数量,总购买装备数量)

	break;
end








