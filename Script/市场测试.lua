
--[[***********市场购买测试***************
                   _ooOoo_
                  o8888888o
                  88" . "88
                  (| -_- |)
                  O\  =  /O
               ____/`---'\____
             .'  \\|     |//  `.
            /  \\|||  :  |||//  \
           /  _||||| -:- |||||-  \
           |   | \\\  -  /// |   |
           | \_|  ''\---/''  |   |
           \  .-\__  `-`  ___/-. /
         ___`. .'  /--.--\  `. . __
      ."" '<  `.___\_<|>_/___.'  >'"".
     | | :  `- \`.;`\ _ /`;.`/ - ` : | |
     \  \ `-.   \_ __\ /__ _/   .-` /  /
======`-.____`-.___\_____/___.-`____.-'======
                   `=---='
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         佛祖保佑       永无BUG
]]


--市场菜单配置
local tMarketMenuConfigs={
	{ID=1,NAME="推荐",BTN_POINT={332,107}},
	{ID=2,NAME="道具",BTN_POINT={463,107}},
	{ID=3,NAME="武器",BTN_POINT={594,107}},
	{ID=4,NAME="盔甲",BTN_POINT={726,107}},
	{ID=5,NAME="战马",BTN_POINT={856,107}}
}

--购买按钮位置
local tBuyBtnPoint={
	{x=630,y=255},
	{x=900,y=255},
	{x=1170,y=255},
	{x=630,y=405},
	{x=900,y=405},
	{x=1170,y=405},
	{x=630,y=550},
	{x=900,y=550},
	{x=1170,y=550},
	{x=630,y=700},
	{x=900,y=700},
	{x=1170,y=700}
}

--获得市场排列数据
--@param nMenuID 菜单ID
--@param nNextMenuID 默认为0
local function GetStoreSortInfo(nMenuID,nNextMenuID)
	local szScript=[[return (function()
		local szData=""
		local nMenuID=%d
		local nNextMenuID=%d
		local tData=StoreData:GetInstance():GetStoreSortInfo(nMenuID,nNextMenuID)
		szData=table.serialize(tData)
		return szData
	end)()]]
	local szData=nil
	repeat
		szData=dm:ExecuteGameLuaRet(string.format(szScript,nMenuID,nNextMenuID))
	until szData~=nil
	return loadstring(szData)()
end

--获取市场物品数据
local function GetItemInfoByType(nTypeID,nTimeBoundIndex)
	local szScript=[[return (function()
		local szData=""
		local nTypeID=%d
		local nTimeBoundIndex=%d
		local tData=StoreData:GetInstance():GetStoreInfoByType(nTypeID,nTimeBoundIndex)
		tData.strName = GetStrDB_lua(tData.strName)
		szData=table.serialize(tData)
		return szData
	end)()]]
	local szData=nil
	repeat
		szData=dm:ExecuteGameLuaRet(string.format(szScript,nTypeID,nTimeBoundIndex))
	until szData~=nil
	return loadstring(szData)()
end

--购买物品
local function BuyItem(nItemId,nLine,nList)
	
	
	
end

local function 市场购买测试()

	ndlog("------------------【市场购买】测试开始------------------")
	
	for m=1,#tMarketMenuConfigs do
	
		tMenuConfig = tMarketMenuConfigs[m]
		dm:MoveTo(tMenuConfig.BTN_POINT[1],tMenuConfig.BTN_POINT[2])
		dm:LeftClick()
		
		--根据菜单ID获取市场物品排列信息
		local tStoreSortInfo = GetStoreSortInfo(tMenuConfig.ID,0)
		
		dm:Delay(1000)
		
		--所有物品数量
		local nTotalItem = #tStoreSortInfo
		
		--所有页数
		local nTotalPage = math.floor((nTotalItem - 1)/12) + 1
		
		local nCurrItem = 1
		
		for nCurrPage=1,nTotalPage do
			--每一页12件商品
			for nGrid=1,12 do
			
				for idx=1,#tStoreSortInfo[nCurrItem] do
					
					--根据物品类型ID获取物品详细信息
					local nItemTypeID = tStoreSortInfo[nCurrItem][idx].id
					tItemInfo = GetItemInfoByType(nItemTypeID,tStoreSortInfo[nCurrItem][idx].unTimeBoundIndex)
					
					local bCanBuy = true
					
					--科技线物品
					if tItemInfo.bTechEquip then
						if tItemInfo.bTechOpen then
							if tItemInfo.bTechBuy then
								bCanBuy = false
								ndlog("[%s],%d页,物品:%s,ItemTypeID:%d,已购买",tMenuConfig.NAME,nCurrPage,tItemInfo.strName,tItemInfo.idItemType)
							end
						else
							bCanBuy = false
							ndlog("[%s],%d页,物品:%s,ItemTypeID:%d,未解锁",tMenuConfig.NAME,nCurrPage,tItemInfo.strName,tItemInfo.idItemType)
						end
					end
					
					if bCanBuy then
						--时效
						local szExpDate = ""
						if tItemInfo.unTimeBound == 0 then
							szExpDate = "永久"
						elseif tItemInfo.unTimeBound < 24 then
							szExpDate = tostring(tItemInfo.unTimeBound).."小时"
						else
							szExpDate = tostring(tItemInfo.unTimeBound/24).."天"
						end
						
						--消耗的值
						local nDuductValue = 0
						
						--购买需要的货币
						local nNeedCurrency = 0
						local szCurrency = ""
						if tItemInfo.unSellEMoney > 0 then
							szCurrency = "金币"
							nDuductValue = 获取玩家金币()
							nNeedCurrency = tItemInfo.unSellEMoney
						elseif tItemInfo.unSellMoney > 0 then
							szCurrency = "铜币"
							nDuductValue = 获取玩家铜币()
							nNeedCurrency = tItemInfo.unSellMoney
						elseif tItemInfo.unSellExploit > 0 then
							szCurrency = "军功"
							nDuductValue = 获取玩家军功()
							nNeedCurrency = tItemInfo.unSellExploit
						elseif tItemInfo.unSellGeneralexp > 0 then
							szCurrency = "声望"
							nDuductValue = 获取玩家声望()
							nNeedCurrency = tItemInfo.unSellGeneralexp
						end
						
						local szBuyResult = ""
						
						--购买物品
						dm:MoveTo(tBuyBtnPoint[nGrid].x,tBuyBtnPoint[nGrid].y)
						dm:LeftClick()
						if MyCmpColor(517,211,"d9d1ba",3000)==0 then
							ndlog("点击购买按钮等待弹出购买窗口失败,[%s],%d页,物品:%s",tMenuConfig.NAME,nCurrPage,tItemInfo.strName)
							szBuyResult = "---->购买失败"
						else
							--点击对应的时效按钮
							dm:MoveTo(535+(idx-1)*45,445)
							dm:LeftClick()
							
							--点击购买
							repeat
								dm:MoveTo(593,583)
								dm:LeftClick()
								dm:Delay(500)
							until GameFrameIsVisible("Market_Buy")==false
							
							--等待购买成功提示出现
							if WaitGameFrameIsVisible("TextFrame_Market_Prompt") then
								dm:ExecuteGameLua('TK_FrameHide_FadeOut("TextFrame_Market_Prompt",0)')
								szBuyResult = "购买成功"
							else
								szBuyResult = "---->购买失败"
							end
							
						end
						
						--购买之前的值减去购买之后的值就是消耗掉得货币
						if tItemInfo.unSellEMoney > 0 then
							nDuductValue = nDuductValue - 获取玩家金币()
						elseif tItemInfo.unSellMoney > 0 then
							nDuductValue = nDuductValue - 获取玩家铜币()
						elseif tItemInfo.unSellExploit > 0 then
							nDuductValue = nDuductValue - 获取玩家军功()
						elseif tItemInfo.unSellGeneralexp > 0 then
							nDuductValue = nDuductValue - 获取玩家声望()
						end
						
						if nNeedCurrency~=nDuductValue then
							szBuyResult = "---->价格异常"
						end
						
						ndlog("%s[%s],%d页,物品:%s,ItemTypeID:%d,时效:%s,%s/消耗:%d/%d",szBuyResult,
							tMenuConfig.NAME,nCurrPage,tItemInfo.strName,tItemInfo.idItemType,szExpDate,szCurrency,nNeedCurrency,nDuductValue)
						
					end
					
				end
				
				nCurrItem = nCurrItem + 1
				if nCurrItem > nTotalItem then
					break;
				end
			end
			
			--点击下一页
			if nCurrItem < nTotalItem then
				dm:MoveTo(905,728)
				dm:LeftClick()
				dm:Delay(1000)
			end
			
		end
		
	end
	
	ndlog("------------------【市场购买】测试结束------------------")
end


while true do

	g_bMarketTest=true
	require "升级测试"
	g_bMarketTest=false
	
	--如果市场界面没有显示，那么打开
	if not GameFrameIsVisible("Market") then
		-- 移动到市场按钮并点击
		dm:MoveTo(778,19)
		dm:LeftClick()

		-- 判断窗口是否打开
		if WaitGameFrameIsVisible("Market")==false then
			ndlog("打开市场窗口失败")
			break;
		end
		
		dm:Delay(1000)
		
	end
	
	市场购买测试()

	break;
end








